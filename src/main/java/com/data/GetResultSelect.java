package com.data;

import com.logging.LoggerManager;
import com.mongodb.client.*;
import org.bson.Document;
import org.bson.conversions.Bson;
import org.slf4j.Logger;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import static com.mongodb.client.model.Accumulators.sum;
import static com.mongodb.client.model.Aggregates.*;
import static com.mongodb.client.model.Filters.eq;
import static com.mongodb.client.model.Projections.*;
import static com.mongodb.client.model.Sorts.descending;

import com.mongodb.client.AggregateIterable;

public class GetResultSelect {
    private static final Logger LOGGER = LoggerManager.getLogger();
    MongoDatabase database;

    public GetResultSelect(MongoDatabase database) {
        this.database = database;
    }

    int getIdOfValueFromCollection(String collectionName, String value) {
        int result = 0;
        MongoCollection<Document> collection = database.getCollection(collectionName);
        Bson filter = eq("name", value);

        Document document = collection.find(filter).first();
        if (document != null && document.containsKey("_id")) {
            result = Integer.parseInt(document.get("_id").toString());
        }
        return result;
    }

    Document getDocumentFromCollectionById(String collectionName, int id) {
        MongoCollection<Document> collection = database.getCollection(collectionName);
        return collection.find(eq("_id", id)).first();
    }

    public List<Bson> resultSelect(int typeId) {

        var generalStart1 = System.nanoTime();
        String totalBalance = "totalBalance";

        List<Bson> pipeline = new ArrayList<>();
        Bson matchType = match(eq("product_type_id", typeId));
        Bson group = group("$store_id", sum(totalBalance, "$remain"));
        Bson sort = sort(descending(totalBalance));
        Bson limit = limit(1);

        pipeline.add(matchType);
        pipeline.add(group);
        pipeline.add(sort);
        pipeline.add(limit);

        Bson lookupStore = lookup("store", "_id", "_id", "store");
        Bson unwindStore = unwind("$store");
        Bson projectStore = project(fields(
                excludeId(),
                computed("address", "$store.location"),
                computed("storeName", "$store.name"),
                include(totalBalance, "product_type_id")
        ));

        pipeline.add(lookupStore);
        pipeline.add(unwindStore);
        pipeline.add(projectStore);

        var generalEnd1 = System.nanoTime();
        var totalAmountOfWork1 = (generalEnd1 - generalStart1) / 1_000_000;
        LOGGER.info("Searching total amount of products of one Type taken: {} millis", totalAmountOfWork1);

        return pipeline;
    }

    public String getResultDocumentFromSelect(String type) {

        int resultTypeId = getIdOfValueFromCollection("type", type);
        MongoCollection<Document> remainCollection = database.getCollection("remain");
        List<Bson> pipeline = resultSelect(resultTypeId);

        Document typeDocument = getDocumentFromCollectionById("type", resultTypeId);

        var generalStart1 = System.nanoTime();

        AggregateIterable<Document> results = remainCollection.aggregate(pipeline);
        Document document = results.first();

        var generalEnd1 = System.nanoTime();
        var totalAmountOfWork1 = (generalEnd1 - generalStart1) / 1_000_000;
        LOGGER.info("GETTING RESULT FROM DB taken: {} millis", totalAmountOfWork1);

        String typeName = (String) typeDocument.get("name");
        String  balance = Objects.requireNonNull(document).get("totalBalance").toString();
        String  address = document.get("address").toString();
        String  storeName = document.get("storeName").toString();



        return "Залишок=" + balance +
                ", Крамниця=" + storeName +
                ", Адреса=" + address +
                ", тип продукту=" + typeName;
    }
}
