package com.data;

import com.logging.LoggerManager;
import com.model.ProductDTO;
import com.model.ProductTypeDTO;
import com.model.RemainDTO;
import com.model.StoreDTO;
import com.utils.LoadProperties;
import com.validate.ValidateProduct;
import org.slf4j.Logger;

import java.util.ArrayList;
import java.util.List;

import static com.randomgenerating.RandomGenerator.getRandomInt;
import static com.randomgenerating.RandomGenerator.getRandomString;

public class DataGenerating {
    private static final Logger LOGGER = LoggerManager.getLogger();
    private static final String COUNT_VALID_PRODUCT = "count_valid_product";
    private static final String MIN_PRODUCT_AMOUNT_GENERATE = "min_product_amount_generate";
    private static final String MAX_PRODUCT_AMOUNT_GENERATE = "max_product_amount_generate";
    private static final String REMAIN_AMOUNT = "remain_amount";
    int countValidProduct;
    int countInvalidProduct;
    ValidateProduct validator;
    LoadProperties properties;

    public DataGenerating(ValidateProduct validateProduct, LoadProperties properties) {
        this.properties = properties;
        this.validator = validateProduct;
    }


    public List<ProductDTO> productDTOList(List<ProductTypeDTO> productTypeDTOList) {

        int amountValidProduct = Integer.parseInt(properties.getProperty(COUNT_VALID_PRODUCT));
        List<ProductDTO> list = new ArrayList<>(amountValidProduct);

        int productTypeSize = productTypeDTOList.size();

        LOGGER.info("Begin filling table list");
        var startProduct = System.nanoTime();


        int countProgress = 1;
        int count = 1;
        while (count <= amountValidProduct) {
            int idProductType = getRandomInt(1, productTypeSize);

            String productName = getRandomString(1, 30);
            ProductDTO product = new ProductDTO(count, productName, idProductType);
            if (validator.validate(new ProductDTO(count, productName, idProductType))) {
                countValidProduct++;
                count++;
                list.add(product);
            } else {
                countInvalidProduct++;
            }

            countProgress++;
            loadProgress(amountValidProduct, countProgress, 5001);

        }

        var endProduct = System.nanoTime();
        var resultProduct = (endProduct - startProduct) / 1_000_000;
        LOGGER.info("Count valid product = {}", countValidProduct);
        LOGGER.info("count invalid product = {}", countInvalidProduct);
        LOGGER.info("filling the list of valid product took: {} millis", resultProduct);

        return list;
    }

    public List<RemainDTO> remainDTOList(List<ProductDTO> productDTOList, List<StoreDTO> storeDTOList) {
        int remainAmount = Integer.parseInt(properties.getProperty(REMAIN_AMOUNT));
        int minAmount = Integer.parseInt(properties.getProperty(MIN_PRODUCT_AMOUNT_GENERATE));
        int maxAmount = Integer.parseInt(properties.getProperty(MAX_PRODUCT_AMOUNT_GENERATE));
        List<RemainDTO> list = new ArrayList<>(remainAmount);

        int countProgress = 1;

        LOGGER.info("Begin generate list remain");
        var startRemain = System.nanoTime();

        for (int i = 0; i < remainAmount; i++) {
            int amount = getRandomInt(minAmount, maxAmount);
            int productId = getRandomInt(0, productDTOList.size() - 1);
            int storeId = getRandomInt(1, storeDTOList.size());

            ProductDTO productDTO = productDTOList.get(productId);

            RemainDTO remainDTO = new RemainDTO(productDTO.getId(), storeId, amount, productDTO.getProductTypeId());
            list.add(remainDTO);

            countProgress++;
            loadProgress(remainAmount, countProgress, 50001);
        }

        var endRemain = System.nanoTime();
        var resultRemain = (endRemain - startRemain) / 1_000_000;
        LOGGER.info("generating {} products in the table remain took: {} millis", remainAmount, resultRemain);

        return list;
    }

    private void loadProgress(int amountValidProduct, int countProgress, int numberForDivide) {
        if (countProgress % numberForDivide == 0) {
            double progress = (double) countProgress / amountValidProduct * 100;
            LOGGER.info("Progress is {}%", (int) progress);
        }
    }
}
