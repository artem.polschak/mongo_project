package com.data;

import com.logging.LoggerManager;
import com.model.*;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import org.bson.Document;
import org.slf4j.Logger;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class TableDataLoadToMongoDB {
    private static final Logger LOGGER = LoggerManager.getLogger();
    MongoDatabase database;
    long startCountRemainTime;
    long endCountRemainTime;
    long resultCountRemainTime;
    long rps;
    int remainDTOSize;


    public TableDataLoadToMongoDB(MongoDatabase database) {
        this.database = database;
    }

    public void fillProductTypeTable(List<ProductTypeDTO> productTypeDTOList, String nameCollection) {
        LOGGER.info("Begin filling table product_type ");
        var start = System.nanoTime();

        MongoCollection<Document> collection = database.getCollection(nameCollection);

        List<Document> documents = convertProductTypeDTOList(productTypeDTOList);

        collection.insertMany(documents);
        var end = System.nanoTime();
        var result = (end - start) / 1_000_000;
        LOGGER.info("Filling the table product_type took: {} millis", result);
        LOGGER.info("Size of product_type: {} ", documents.size());
    }

    List<Document> convertProductTypeDTOList(List<ProductTypeDTO> productTypeDTOList) {
        List<Document> documents = new ArrayList<>(productTypeDTOList.size());
        for (ProductTypeDTO typeDTO : productTypeDTOList) {
            Document document = new Document("_id", typeDTO.getId())
                    .append("name", typeDTO.getName());
            documents.add(document);
        }
        return documents;
    }

    public void fillStoreTable(List<StoreDTO> storeDTOList, String nameCollection) {
        LOGGER.info("Begin filling table store ");
        var start = System.nanoTime();
        MongoCollection<Document> collection = database.getCollection(nameCollection);
        List<Document> documents = convertStoreDTOList(storeDTOList);

        collection.insertMany(documents);
        var end = System.nanoTime();
        var result = (end - start) / 1_000_000;
        LOGGER.info("Filling the table store took: {} millis", result);
        LOGGER.info("Size of store: {} ", documents.size());
    }

    List<Document> convertStoreDTOList(List<StoreDTO> storeDTOList) {
        List<Document> documents = new ArrayList<>(storeDTOList.size());
        for (StoreDTO storeDTO : storeDTOList) {
            Document document = new Document("_id", storeDTO.getId())
                    .append("name", storeDTO.getName())
                    .append("location", storeDTO.getLocation());
            documents.add(document);
        }
        return documents;
    }

    public void fillProductTable(List<ProductDTO> productDTOList, String nameCollection) {
        LOGGER.info("Begin filling table product");
        var start = System.nanoTime();
        MongoCollection<Document> collection = database.getCollection(nameCollection);
        List<Document> documents = convertProductDTOList(productDTOList);

        collection.insertMany(documents);
        var end = System.nanoTime();
        var result = (end - start) / 1_000_000;
        LOGGER.info("Filling the table product took: {} millis", result);
        LOGGER.info("Size of products: {} ", documents.size());
    }

    List<Document> convertProductDTOList(List<ProductDTO> productDTOList) {
        List<Document> documents = new ArrayList<>(productDTOList.size());
        for (ProductDTO productDTO : productDTOList) {
            Document document = new Document("_id", productDTO.getId())
                    .append("name", productDTO.getName())
                    .append("product_type_id", productDTO.getProductTypeId());
            documents.add(document);
        }
        return documents;
    }

    Document convertRemainDTOtoDocument(RemainDTO remainDTO) {
        return new Document("product_id", remainDTO.getProductId())
                .append("remain", remainDTO.getRemain())
                .append("store_id", remainDTO.getStoreId())
                .append("product_type_id", remainDTO.getProductTypeId());
    }

    public void fillRemainTable(List<RemainDTO> remainDTOList, String nameCollection) {
        LOGGER.info("Begin filling table remain");
        startCountRemainTime = System.nanoTime();

        MongoCollection<Document> collection = database.getCollection(nameCollection);

        LOGGER.info("Finish prepare to insert into collection remain");
        LOGGER.info("Begin inserting data into collection remain");

        LinkedList<Document> linkedList = new LinkedList<>();

        int count = 1;
        int maxSize = remainDTOList.size();
        remainDTOSize = remainDTOList.size();
        for (RemainDTO remainDTO : remainDTOList) {
            linkedList.add(convertRemainDTOtoDocument(remainDTO));
            if (count % 10000 == 0) {
                collection.insertMany(linkedList);
                linkedList.clear();
            }
            loadProgress(maxSize, count, 50000);
            count++;
        }
        if (!linkedList.isEmpty()) {
            collection.insertMany(linkedList);
            linkedList.clear();
        }

        LOGGER.info("Finish inserting data into collection remain");
        endCountRemainTime = System.nanoTime();
        resultCountRemainTime = (endCountRemainTime - startCountRemainTime) / 1_000_000;


    }

    public void getRpsFromRemain() {
        rps = remainDTOSize / (resultCountRemainTime / 1000);
        LOGGER.info("RPS generating data in collection Remain = {}", rps);
    }

    private void loadProgress(int amountValidProduct, int countProgress, int numberForDivide) {
        if (countProgress % numberForDivide == 0) {
            double progress = countLoadProgress(amountValidProduct, countProgress);
            LOGGER.info("Progress is {}%", (int) progress);
        }
    }

    double countLoadProgress(int amountValidProduct, double countProgress) {
        return  countProgress / amountValidProduct * 100;
    }

    public void dropCollection(String name) {
        MongoCollection<Document> collection = database.getCollection(name);
        collection.drop();
    }
}



