package com.csv_parser;

import com.logging.LoggerManager;
import com.model.StoreDTO;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.slf4j.Logger;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class StoreCSVParser {
    private static final Logger LOGGER = LoggerManager.getLogger();

    public List<StoreDTO> parseStoreCSV(String filePath) {
        int id = 1;
        String name;
        String location;

        List<StoreDTO> list = new ArrayList<>();

        LOGGER.info("Begin read data from {} to list", filePath);
        var startStore = System.nanoTime();

        try (CSVParser csvStore = new CSVParser(new FileReader(filePath),
                CSVFormat.DEFAULT.withHeader())) {
            for (CSVRecord reader : csvStore) {
                name = reader.get("name");
                location = reader.get("location");
                list.add(new StoreDTO(id++, name, location));
            }
        } catch (FileNotFoundException e) {
            LOGGER.error("File {} does not find {} {}", filePath, e.getMessage(), e);
        } catch (IOException e) {
            LOGGER.error("Error while try read data from {} {} {}", filePath, e.getMessage(), e);
        }
        LOGGER.info("Finished read data from {} to list", filePath);
        var endStore = System.nanoTime();
        var resultStore = (endStore - startStore) / 1_000_000;
        LOGGER.info("filling the list with StoreDTO took: {} millis", resultStore);

        return list;
    }
}
