package com.model;

public class RemainDTO implements DTO{
    String product;
    String store;
    int remain;
    int productId;
    int storeId;
    int productTypeId;

    public RemainDTO(String product, String store, int remain) {
        this.product = product;
        this.store = store;
        this.remain = remain;
    }

    public RemainDTO(int productId, int storeId, int remain, int productTypeId) {
        this.remain = remain;
        this.productId = productId;
        this.storeId = storeId;
        this.productTypeId = productTypeId;
    }

    public RemainDTO(int productId, int storeId, int remain) {
        this.productId = productId;
        this.storeId = storeId;
        this.remain = remain;
    }

    public int getProductTypeId() {
        return productTypeId;
    }

    public void setProductTypeId(int productTypeId) {
        this.productTypeId = productTypeId;
    }

    public int getProductId() {
        return productId;
    }

    public void setProductId(int productId) {
        this.productId = productId;
    }

    public int getStoreId() {
        return storeId;
    }

    public void setStoreId(int storeId) {
        this.storeId = storeId;
    }

    public String getProduct() {
        return product;
    }

    public void setProduct(String product) {
        this.product = product;
    }

    public String getStore() {
        return store;
    }

    public void setStore(String store) {
        this.store = store;
    }

    public int getRemain() {
        return remain;
    }

    public void setRemain(int remain) {
        this.remain = remain;
    }
}
