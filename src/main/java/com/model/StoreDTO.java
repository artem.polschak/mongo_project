package com.model;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;

public class StoreDTO implements DTO {
    int id;
    String name;
    String location;

    public StoreDTO(int id, String store, String location) {
        this.id = id;
        this.name = store;
        this.location = location;
    }

    public StoreDTO(String store, String location) {
        this.name = store;
        this.location = location;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @NotNull(message = "Name is compulsory")
    @NotBlank(message = "Name is compulsory")
    @Size(min = 1, max = 30, message = "Size must have minimum two letters")
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @NotNull(message = "Name is compulsory")
    @NotBlank(message = "Name is compulsory")
    @Size(min = 1, max = 40, message = "Size must have minimum two letters")
    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    @Override
    public String toString() {
        return "StoreDTO{" +
                "store='" + name + '\'' +
                ", location='" + location + '\'' +
                '}';
    }
}
