package com.model;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;

public class ProductDTO implements DTO {
    String name;
    int productTypeId;
    int id;

    public ProductDTO(String product, int productTypeId) {
        this.name = product;
        this.productTypeId = productTypeId;
    }

    public ProductDTO(int id, String name, int productTypeId) {
        this.id = id;
        this.name = name;
        this.productTypeId = productTypeId;

    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
    public ProductDTO(String product) {
        this.name = product;
    }

    public int getProductTypeId() {
        return productTypeId;
    }

    public void setProductTypeId(int productTypeId) {
        this.productTypeId = productTypeId;
    }

    public void setName(String name) {
        this.name = name;
    }

    @NotNull(message = "Name is compulsory")
    @NotBlank(message = "Name is compulsory")
    @Size(min = 2, message = "Size must have minimum two letters")
    public String getName() {
        return name;
    }
}
