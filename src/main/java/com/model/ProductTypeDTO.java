package com.model;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;

public class ProductTypeDTO implements DTO {
    int id;
    String name;
    public ProductTypeDTO(String product) {
        this.name = product;
    }

    public ProductTypeDTO(int id, String productType) {
        this.id = id;
        this.name = productType;
    }

    public ProductTypeDTO() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @NotNull(message = "Name is compulsory")
    @NotBlank(message = "Name is compulsory")
    @Size(min = 3, message = "Size must have minimum two letters")
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "ProductTypeDTO{" +
                "productType='" + name + '\'' +
                '}';
    }
}
