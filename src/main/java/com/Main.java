package com;

import com.connection.MongoDBConnection;
import com.data.DataGenerating;
import com.csv_parser.ProductTypeCSVParser;
import com.csv_parser.StoreCSVParser;
import com.data.GetResultSelect;
import com.data.TableDataLoadToMongoDB;
import com.logging.LoggerManager;
import com.model.ProductDTO;
import com.model.ProductTypeDTO;
import com.model.RemainDTO;
import com.model.StoreDTO;
import com.mongodb.client.MongoDatabase;
import com.utils.LoadProperties;
import com.validate.ValidateProduct;
import org.slf4j.Logger;

import java.util.List;

public class Main {
    private static final Logger LOGGER = LoggerManager.getLogger();
    private static final String CONFIG = "config.properties";
    private static final String CONNECTION = "connection.properties";
    private static final String STORE_CSV_FILE_NAME = "storeCSVPath";
    private static final String PRODUCT_TYPE_CSV_FILE_NAME = "productTypeCSVPath";
    private static final String DATABASE_NAME = "dataBaseName";
    private static final String TYPE = "type";
    private static final String URI = "uri";
    static LoadProperties configProperty;
    static LoadProperties connectionProperty;
    static boolean createDDLTablesAndIndexes = true;

    public static void main(String[] args) {

        configProperty = new LoadProperties(CONFIG);
        LOGGER.info("Start Load Property {} from {}", CONFIG, Main.class.getName());
        configProperty.loadProperties();
        LOGGER.info("Finish Load Property {} from {}", CONFIG, Main.class.getName());

        connectionProperty = new LoadProperties(CONNECTION);
        LOGGER.info("Start Load Property {} from {}", CONNECTION, Main.class.getName());
        connectionProperty.loadProperties();
        LOGGER.info("Finish Load Property {} from {}", CONNECTION, Main.class.getName());

        switcherCreateTablesAndIndexes(args);

        String uri = connectionProperty.getProperty(URI);
        String dataBaseName = connectionProperty.getProperty(DATABASE_NAME);

        MongoDBConnection mongoDBConnection = new MongoDBConnection(uri, dataBaseName);
        MongoDatabase mongoDatabase = mongoDBConnection.getMongoDatabase();

        var generalStart = System.nanoTime();
        LOGGER.info("The main method start: from {}", Main.class.getName());

        ProductTypeCSVParser productTypeParser = new ProductTypeCSVParser();
        StoreCSVParser storeParser = new StoreCSVParser();
        DataGenerating dataGenerating = new DataGenerating(new ValidateProduct(), configProperty);
        TableDataLoadToMongoDB dataLoadToMongoDB = new TableDataLoadToMongoDB(mongoDatabase);
        GetResultSelect result = new GetResultSelect(mongoDatabase);

        List<ProductTypeDTO> productTypeList = productTypeParser.parseProductTypeCSV(configProperty.getProperty(PRODUCT_TYPE_CSV_FILE_NAME));
        List<StoreDTO> storeList = storeParser.parseStoreCSV(configProperty.getProperty(STORE_CSV_FILE_NAME));
        List<ProductDTO> productList = dataGenerating.productDTOList(productTypeList);
        List<RemainDTO> remainDTOList = dataGenerating.remainDTOList(productList, storeList);

        dataLoadToMongoDB.dropCollection("type");
        dataLoadToMongoDB.dropCollection("store");
        dataLoadToMongoDB.dropCollection("product");
        dataLoadToMongoDB.dropCollection("remain");

        dataLoadToMongoDB.fillProductTypeTable(productTypeList, "type");
        dataLoadToMongoDB.fillStoreTable(storeList, "store");
        dataLoadToMongoDB.fillProductTable(productList, "product");
        dataLoadToMongoDB.fillRemainTable(remainDTOList, "remain");
        dataLoadToMongoDB.getRpsFromRemain();

        LOGGER.info("size of productTypeList={}", productTypeList.size());
        LOGGER.info("size of storeList={}", storeList.size());
        LOGGER.info("size of productList={}", productList.size());
        LOGGER.info("size of remainDTOList={}", remainDTOList.size());

        var startSelect = System.nanoTime();
        String resultSet = result.getResultDocumentFromSelect(returnDType());
        LOGGER.info("{}", resultSet);
        mongoDBConnection.closeConnection();
        var endSelect = System.nanoTime();
        var resultCountSelect = (endSelect - startSelect) / 1_000_000;
        LOGGER.info("Work of Searching Class took : {} millis", resultCountSelect);
        var generalEnd = System.nanoTime();
        var totalAmountOfWork = (generalEnd - generalStart) / 1_000_000;
        LOGGER.info("General work of the program took: {} millis", totalAmountOfWork);
    }

    private static void switcherCreateTablesAndIndexes(String[] args) {
        if (args.length == 1) {
            String bool = args[0];
            if (bool.equals("true") || bool.equals("false")) {
                createDDLTablesAndIndexes = Boolean.parseBoolean(bool);
            }
        }
    }

    public static String returnDType() {
        return System.getProperty(TYPE, "мед");
    }
}
