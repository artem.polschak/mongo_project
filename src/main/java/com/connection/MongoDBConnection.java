package com.connection;

import com.logging.LoggerManager;
import com.mongodb.*;
import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoClients;
import com.mongodb.client.MongoDatabase;
import org.slf4j.Logger;

public class MongoDBConnection {
    private static final Logger LOGGER = LoggerManager.getLogger();
    MongoClient mongoClient;
    MongoDatabase mongoDatabase;

    public MongoDBConnection(String uri, String databaseName) {


        ConnectionString connectionString = new ConnectionString(uri);

        ServerApi serverApi = ServerApi.builder()
                .version(ServerApiVersion.V1)
                .build();

        MongoClientSettings settings = MongoClientSettings.builder()
                .applyConnectionString(connectionString)
                .serverApi(serverApi)
                .build();

        mongoClient = MongoClients.create(settings);
        mongoDatabase = mongoClient.getDatabase(databaseName);
        LOGGER.info("init connection to dataBase MongoDB {}", uri);
    }

    public MongoClient getMongoClient() {
        return mongoClient;
    }

    public MongoDatabase getMongoDatabase() {
        return mongoDatabase;
    }

    public void closeConnection() {
        if (mongoClient != null) {
            mongoClient.close();
            LOGGER.info("close connection to dataBase MongoDB ");
        }
    }
}

