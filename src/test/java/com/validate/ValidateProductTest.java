package com.validate;

import com.model.ProductDTO;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class ValidateProductTest {

    private ValidateProduct validateProduct;
    private ProductDTO validProductDTO;
    private ProductDTO invalidProductDTO;

    @BeforeEach
    public void setUp() {
        validateProduct = new ValidateProduct();
        validProductDTO = new ProductDTO("Valid Product", 1);
        invalidProductDTO = new ProductDTO("I", 2);
    }

    @Test
     void testValidateWithValidProductDTO() {
        boolean result = validateProduct.validate(validProductDTO);
        Assertions.assertTrue(result);
    }

    @Test
     void testValidateWithInvalidProductDTO() {
        boolean result = validateProduct.validate(invalidProductDTO);
        Assertions.assertFalse(result);
    }
}

