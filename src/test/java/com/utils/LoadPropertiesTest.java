package com.utils;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class LoadPropertiesTest {

    private LoadProperties loader;

    @BeforeEach
    public void setUp() {
        loader = new LoadProperties("config.properties");
        loader.loadProperties();
    }

    @Test
    void testGetProperty() {
        String propertyValue = loader.getProperty("key");
        Assertions.assertEquals("value", propertyValue);
    }

    @Test
    void testSetProperties() {
        loader.setProperties("newKey", "newValue");
        String propertyValue = loader.getProperty("newKey");
        Assertions.assertEquals("newValue", propertyValue);
    }

    @Test
    void testGetPathProperty() {
        String path = loader.getPathProperty("config.properties");
        Assertions.assertNotNull(path);
    }

}
