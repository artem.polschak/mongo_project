package com.data;

import static org.junit.jupiter.api.Assertions.*;

import com.mongodb.client.AggregateIterable;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import org.bson.Document;
import org.bson.conversions.Bson;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentMatchers;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
@ExtendWith(MockitoExtension.class)
class GetResultSelectTest {
    @Mock
    private MongoDatabase database;
    @Mock
    private MongoCollection<Document> collection;
    @Mock
    private AggregateIterable<Document> aggregateIterable;

    private GetResultSelect getResultSelect;

    @BeforeEach
    void setUp() {
        getResultSelect = new GetResultSelect(database);
    }

    @Test
    void getIdOfValueFromCollectionTest() {
        String collectionName = "col";
        String value = "value";
        when(database.getCollection(collectionName)).thenReturn(collection);

        Document document = new Document("_id", 1).append("name", value);
        FindIterable<Document> findIterable = Mockito.mock(FindIterable.class);
        when(collection.find(any(Bson.class))).thenReturn(findIterable);
        when(findIterable.first()).thenReturn(document);

        Integer result = getResultSelect.getIdOfValueFromCollection(collectionName, value);
        Integer expected = 1;
        assertEquals(expected, result);
    }

}

