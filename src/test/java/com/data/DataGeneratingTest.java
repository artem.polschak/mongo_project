package com.data;

import com.model.ProductDTO;
import com.model.ProductTypeDTO;
import com.model.RemainDTO;
import com.model.StoreDTO;
import com.utils.LoadProperties;
import com.validate.ValidateProduct;
import org.bson.Document;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.powermock.api.mockito.PowerMockito.when;

@ExtendWith(MockitoExtension.class)
class DataGeneratingTest {

    @Mock
    ValidateProduct validator;

    @Mock
    LoadProperties properties;

    DataGenerating dataGenerating;

    @BeforeEach
    public void setUp() {
        dataGenerating = new DataGenerating(validator, properties);
    }

    @Test
    void remainDTOList() {
        List<ProductDTO> productDTOList = new ArrayList<>();
        List<StoreDTO> storeDTOList = new ArrayList<>();

        for (int i = 0; i < 10; i++) {
            productDTOList.add(new ProductDTO("first" + i));
        }

        for (int i = 0; i < 10; i++) {
            storeDTOList.add(new StoreDTO("first" + i, "location"));
        }

        int remainAmount = 10;

        when(properties.getProperty("remain_amount")).thenReturn(String.valueOf(remainAmount));
        when(properties.getProperty("min_product_amount_generate")).thenReturn("1");
        when(properties.getProperty("max_product_amount_generate")).thenReturn("10");

        List<RemainDTO> result = dataGenerating.remainDTOList(productDTOList, storeDTOList);

        assertEquals(remainAmount, result.size());
    }

    @Test
    void productDTOList() {
        List<ProductTypeDTO> productTypeDTOList = new ArrayList<>();

        for (int i = 0; i < 10; i++) {
            productTypeDTOList.add(new ProductTypeDTO("first" + i));
        }

        int amountValidProduct = 10;

        when(properties.getProperty("count_valid_product")).thenReturn(String.valueOf(amountValidProduct));
        when(validator.validate(any(ProductDTO.class))).thenReturn(true);

        List<ProductDTO> result = dataGenerating.productDTOList(productTypeDTOList);

        assertEquals(amountValidProduct, result.size());
    }
}