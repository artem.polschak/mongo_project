package com.data;

import com.model.*;
import com.mongodb.client.MongoClient;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.ArgumentMatchers;

import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import org.bson.Document;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class TableDataLoadToMongoDBTest {
    @Mock
    private MongoClient client;

    @Mock
    private MongoDatabase database;

    @Mock
    private MongoCollection<Document> collection;

    private TableDataLoadToMongoDB dataLoader;

    @BeforeEach
    public void setUp() {
        dataLoader = new TableDataLoadToMongoDB(/*client,*/ database);
    }

    @AfterEach
    public void tearDown() {
        Mockito.reset(client, database, collection);
    }

    @Test
    void testFillProductTypeTable() {
        List<ProductTypeDTO> productTypeDTOList = new ArrayList<>();
        String collectionName = "product_type";
        Mockito.when(database.getCollection(collectionName)).thenReturn(collection);
        dataLoader.fillProductTypeTable(productTypeDTOList, collectionName);
        verify(collection).insertMany(ArgumentMatchers.anyList());
    }

    @Test
    void testFillStoreTable() {
        List<StoreDTO> storeDTOList = new ArrayList<>();
        String collectionName = "store";
        Mockito.when(database.getCollection(collectionName)).thenReturn(collection);
        dataLoader.fillStoreTable(storeDTOList, collectionName);
        verify(collection).insertMany(ArgumentMatchers.anyList());
    }

    @Test
    void testFillProductTable() {
        List<ProductDTO> productDTOList = new ArrayList<>();
        String collectionName = "product";
        Mockito.when(database.getCollection(collectionName)).thenReturn(collection);
        dataLoader.fillProductTable(productDTOList, collectionName);
        verify(collection).insertMany(ArgumentMatchers.anyList());
    }

    @Test
    void testFillRemainTable() {
        List<RemainDTO> remainDTOList = new LinkedList<>();
        remainDTOList.add(new RemainDTO(1, 1, 1, 1));
        remainDTOList.add(new RemainDTO(2, 2, 2, 2));
        remainDTOList.add(new RemainDTO(3, 3, 3, 3));
        String collectionName = "remain";
        Mockito.when(database.getCollection(collectionName)).thenReturn(collection);
        dataLoader.fillRemainTable(remainDTOList, collectionName);
        verify(collection).insertMany(ArgumentMatchers.anyList());
    }

    @Test
    void testDropCollection() {
        String collectionName = "collection_to_drop";
        Mockito.when(database.getCollection(collectionName)).thenReturn(collection);
        dataLoader.dropCollection(collectionName);
        verify(collection).drop();
    }

    @Test
    void testConvertProductTypeDTOList() {
        List<ProductTypeDTO> productTypeDTOList = Arrays.asList(
                new ProductTypeDTO(1, "Type 1"),
                new ProductTypeDTO(2, "Type 2"),
                new ProductTypeDTO(3, "Type 3")
        );

        List<Document> documents = dataLoader.convertProductTypeDTOList(productTypeDTOList);

        Assertions.assertEquals(3, documents.size());

        Document document1 = new Document("_id", 1).append("name", "Type 1");
        Document document2 = new Document("_id", 2).append("name", "Type 2");
        Document document3 = new Document("_id", 3).append("name", "Type 3");

        Assertions.assertEquals(document1, documents.get(0));
        Assertions.assertEquals(document2, documents.get(1));
        Assertions.assertEquals(document3, documents.get(2));
    }

    @Test
    void testConvertStoreDTOList() {
        List<StoreDTO> storeDTOList = Arrays.asList(
                new StoreDTO(1, "Store 1", "Location 1"),
                new StoreDTO(2, "Store 2", "Location 2"),
                new StoreDTO(3, "Store 3", "Location 3")
        );

        List<Document> documents = dataLoader.convertStoreDTOList(storeDTOList);

        Assertions.assertEquals(3, documents.size());

        Document document1 = new Document("_id", 1).append("name", "Store 1").append("location", "Location 1");
        Document document2 = new Document("_id", 2).append("name", "Store 2").append("location", "Location 2");
        Document document3 = new Document("_id", 3).append("name", "Store 3").append("location", "Location 3");

        Assertions.assertEquals(document1, documents.get(0));
        Assertions.assertEquals(document2, documents.get(1));
        Assertions.assertEquals(document3, documents.get(2));
    }

    @Test
    void testConvertProductDTOList() {
        List<ProductDTO> productDTOList = Arrays.asList(
                new ProductDTO(1, "Product 1", 5),
                new ProductDTO(2, "Product 2", 6),
                new ProductDTO(3, "Product 3", 7)
        );

        List<Document> documents = dataLoader.convertProductDTOList(productDTOList);

        Assertions.assertEquals(3, documents.size());

        Document document1 = new Document("_id", 1).append("name", "Product 1").append("product_type_id", 5);
        Document document2 = new Document("_id", 2).append("name", "Product 2").append("product_type_id", 6);
        Document document3 = new Document("_id", 3).append("name", "Product 3").append("product_type_id", 7);

        Assertions.assertEquals(document1, documents.get(0));
        Assertions.assertEquals(document2, documents.get(1));
        Assertions.assertEquals(document3, documents.get(2));
    }

    @Test
    void testConvertRemainDTOList() {
        RemainDTO remainDTOList = new RemainDTO(1, 2, 5, 6);

        Document documents = dataLoader.convertRemainDTOtoDocument(remainDTOList);

        Assertions.assertEquals(4, documents.size());

        Document document1 = new Document("product_id", 1).append("store_id", 2).append("remain", 5).append("product_type_id", 6);

        Assertions.assertEquals(document1, documents);

    }
    @Test
     void testCountLoadProgress() {

        int amountValidProduct = 100;
        double countProgress = 50.0;

        double result = dataLoader.countLoadProgress(amountValidProduct, countProgress);

        double expectedProgress = countProgress / amountValidProduct * 100;
        Assertions.assertEquals(expectedProgress, result, 0.01);
    }

    @Test
     void testCountLoadProgressWithZeroCountProgress() {

        int amountValidProduct = 100;
        double countProgress = 0.0;

        double result = dataLoader.countLoadProgress(amountValidProduct, countProgress);

        Assertions.assertEquals(0.0, result, 0.01);
    }

}
