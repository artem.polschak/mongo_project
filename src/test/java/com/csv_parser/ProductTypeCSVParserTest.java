package com.csv_parser;

import java.io.*;

import com.model.ProductTypeDTO;
import org.junit.jupiter.api.*;
import org.junit.jupiter.api.io.TempDir;

import java.nio.file.Path;

import org.junit.jupiter.api.BeforeEach;

import java.io.File;
import java.io.IOException;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class ProductTypeCSVParserTest {
    @TempDir
    Path tempDir;

    private File tempFile;

    @BeforeEach
    public void setUp() throws IOException {
        tempFile = createTempCSVFile();
    }

    @AfterEach
    public void tearDown() {
        deleteTempCSVFile();
    }

    private File createTempCSVFile() throws IOException {
        File file = File.createTempFile("temp_product_type_test", ".csv", tempDir.toFile());

        try (FileWriter writer = new FileWriter(file)) {
            writer.append("name\n");
            writer.append("firstType\n");
            writer.append("secondType");
        }
        return file;
    }

    private void deleteTempCSVFile() {
        if (tempFile != null) {
            tempFile.delete();
        }
    }


    @Test
    void testParseProductTypeCSV() {
        String filePath = tempFile.getAbsolutePath();
        ProductTypeCSVParser parser = new ProductTypeCSVParser();
        List<ProductTypeDTO> list = parser.parseProductTypeCSV(filePath);
        String result = "firstType";
        ProductTypeDTO productTypeDTO = list.get(0);

        assertEquals(result, productTypeDTO.getName());
    }


    @Test
    void testExceptionIsThrownFileNotFound() {
        assertThrows(FileNotFoundException.class, () -> {
            String filePath = "nonexistent/example.csv";
            ProductTypeCSVParser parser = new ProductTypeCSVParser();
            List<ProductTypeDTO> list = parser.parseProductTypeCSV(filePath);
            for (ProductTypeDTO dto : list) {
                System.out.println(dto);
            }
            throw new FileNotFoundException();
        });
    }
}