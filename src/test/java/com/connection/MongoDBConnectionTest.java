package com.connection;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import com.mongodb.client.MongoDatabase;

@ExtendWith(MockitoExtension.class)
class MongoDBConnectionTest {
    private static final String URI = "mongodb://127.0.0.1:27023/?retryWrites=true&w=majority";
    private static final String DATABASE_NAME = "ProductStore";

    @Mock
    private MongoDatabase mongoDatabase;

    @InjectMocks
    private MongoDBConnection mongoDBConnection = new MongoDBConnection(URI, DATABASE_NAME);

    @Test
    public void testGetConnection() {
        Assertions.assertEquals(mongoDatabase, mongoDBConnection.getMongoDatabase());
    }
}
